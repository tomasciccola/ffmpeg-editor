
(async () => {

  var requestExample = {
    body: {
      videoAURL: 'https://hbossbucket.s3.amazonaws.com/vid_1.mp4',
      videoBURL: 'https://hbossbucket.s3.amazonaws.com/vid_2.mp4',
      id: '60ad34acbde6326f1a7bbd8b'
    }
  }

  requestExample.body = JSON.stringify(requestExample.body)

  console.log(await require('./index').handler(requestExample, null, console.log))
})()
