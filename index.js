const fs = require('fs')
const path = require('path')
const util = require('util')
const exec = util.promisify(require('child_process').exec)
const pipe = util.promisify(require('stream').pipeline)


const AWS = require('aws-sdk')
const tempy = require('tempy')
const fetch = require('node-fetch')

/*
  
  1. recibí los parámetros del evento { videoAURL, videoBURL, _id }
  2. Obtené los dos videos de S3
  3. Corré script de ffmpeg { video1, video2, output }
  4. Cuando termine --> subí a S3 el archivo resultante
  5. Cuando se suba correctamente --> Obtene la url
  6. Hace un req al server con { _id, completeVideoURL }

  # TODO: Como hacer que devuelva de una el lambda? para que hay un return value? 
  # SUBIRLA A AWS Y ACTIVARLA. Ver como llamarla desde el server ec2.
  (https://stackoverflow.com/questions/35754766/nodejs-invoke-an-aws-lambda-function-from-within-another-lambda-function)
  (https://aws.amazon.com/blogs/media/processing-user-generated-content-using-aws-lambda-and-ffmpeg/)
  (https://www.tsmean.com/articles/aws/the-ultimate-aws-lambda-tutorial-for-nodejs/)
*/

const download = async (url) =>{
  const file = tempy.file()
  const res = await fetch(url)
  if(!res.ok) throw new Error (`error! ${res.statusText}`)
  await pipe(res.body, fs.createWriteStream(file))
  console.log(`downloaded ${url}`)
  return file
}

const upload = async (filename) =>{
  const file = fs.readFileSync(filename)
  let data
  const params = {
    Bucket: 'hugobossbucket',
    Key: path.basename(filename),
    Body: file,
    ContentType:'video/*',
    ACL:'public-read',
    ContentDisposition: 'attachment; filename="unitetocelebrate.mp4"'
  }
  try {
    data = await s3.upload(params).promise()
    console.log('uploaded file to bucket succesfully')
    console.log(data)
  }catch(err){
    throw new Error(`error subiendo archivo: ${err}`)
  }

  return data
}

const notifyServer = async (id,data) => {
  // mandale al server un json con la URL de lo que se subió y el id correspondiente
  const url =  data.Location
  const ENDPOINT = new URL('https://hugo-boss.herokuapp.com/setUniteUserFinalVideo')
  const params = new URLSearchParams({id:id, url:url})

  console.log("mandando req con params", params)

  try {
    const res = await fetch(ENDPOINT, {
      method: 'post',
      body: params
    })
    console.log("response", res.status, res.statusText)

  }catch(err){
    console.log('error haciendo request al server', err)
  }

}


const s3Params = process.env.DEV ? { accessKeyId: process.env.ACCESS_KEY_ID, secretAccessKey: process.env.SECRET_ACCESS_KEY } : {}

const s3 = new AWS.S3(s3Params)

const main = async (body) =>{

  const { videoAURL , videoBURL } = body

  const { id } = body
  
  try {
    // download video inputs
    const videoa = await download(videoAURL)
    const videob = await download(videoBURL)
    const output = tempy.file({extension:'mp4'})

    const ffmpeg = process.env.DEV ? 'ffmpeg' : '/opt/bin/ffmpeg' 
    const CMD = `./edit ${ffmpeg} ${videoa} ${videob} ${output}`

    //run command
    await exec(CMD)
    console.log('converted', output)

    //upload to bucket
    const data = await upload(output)

    // notify server
    await notifyServer(id, data)

    return {
        "isBase64Encoded": false,
        "statusCode": 200,
        "headers": {},
        "body": "gracias"
    }

  }catch(err){
    throw new Error(err)
  }
  

}

exports.handler = async event => {
  const body = JSON.parse(event.body) 
  try {
    return main(body)
  }catch(e){
    console.log("ERROR", e)
    return {
      "isBase64Encoded": false,
      "statusCode":404,
      "headers":{},
      "body": e.toString()
    }
  }
}

